var ipclass;
var bits_disp;
var hosts_bits;
var bitsub;
var sub_bits;
var numhosts;
var numsub;
var mascara;
var ip = new Array();

$(document).ready(function() {
	//Mascara para que se acepten solo 3 numeros
	$('#ip1, #ip2, #ip3, #ip4, #input_opc').mask('9?99');

	//Foco al iniciar
	$('#ip1').focus();
	$('#resultados, #output, #output2, #isClass').hide();

	// Indicar que Input Radio de opcion estará en checked al iniciar
	opc_checked ($('#opc_hosts'));

	// Accion al cambiar valor a los inputs de la IP
	$('#ip1, #ip2, #ip3, #ip4').keyup(function(){
		$('#isClass').hide();
		$('#isClass').fadeIn();
		var val = $('#ip1').val();
		if (this.value > 255 || this.value < 0) {
			ipclass = 'noclass';
			$(this).css('color', 'red');
		}
		else {
			checkclass (val);
			$(this).css('color', 'black');
		}
		changecolor (ipclass);
	});
});

$('a').click(function() {
	console.log("Hola link");
});

function checkclass (val){
	if (val < 128 && val >= 0){
		ipclass = "classA";
	}
	else if (val >= 128 && val < 192) {
		ipclass = "classB";
	}
	else if (val >= 192 && val <= 255) {
		ipclass = "classC";
	}
	else {
		ipclass = "noclass";
	}
}

function changecolor (ipclass){
	if (ipclass == "noclass"){
			$('#isClass').html ("IP inválida");
			$('#isClass').css("background-color", "#E10000");
			$('#btcalc').attr('disabled', 'disabled');
	}
	else if (ipclass == "classA"){
			$('#isClass').html ("Clase A");
			$('#maskClass').html ("<p>Máscara de Subred: 255.0.0.0</p>");
			$('#isClass').css("background-color", "#ECF536");
			$('#btcalc').removeAttr('disabled');
			bits_disp = 24;
	}
	else if (ipclass == "classB"){
			$('#isClass').html ("Clase B");
			$('#maskClass').html ("<p>Máscara de Subred: 255.255.0.0</p>");
			$('#isClass').css("background-color", "#10AF70");
			$('#btcalc').removeAttr('disabled');
			bits_disp = 16;
	}
	else if (ipclass == "classC"){
			$('#isClass').html ("Clase C");
			$('#maskClass').html ("<p>Máscara de Subred: 255.255.255.0</p>");
			$('#isClass').css("background-color", "#287A99");
			$('#btcalc').removeAttr('disabled');
			bits_disp = 8;
	}
}

function opc_checked(radio) {
	radio.attr('checked', 'checked');
	$('#opctx').attr('placeholder', radio.val());
}

function option (input){
	var value = $(input).val();
	$('#opctx').attr('placeholder', value);
	$('#opctx').focus();
}

function check(){

	// Tomar los valores de la IP
	for (var i = 0; i < 4; i++) {
		ip[i] = $('#ip'+(i+1)).val();
	}
	$('#output, #output2').hide();

	if ($('#opc_hosts').is(':checked')) {
		if (ipclass == 'classA' && $('#opctx').val() > Math.pow(2, 24)){
			alert ("El numero de Hosts no debe sobrepasar "+Math.pow(2, 24));
		}
		else if (ipclass == 'classB' && $('#opctx').val() > Math.pow(2, 16)){
			alert ("El numero de Hosts no debe sobrepasar "+Math.pow(2, 16));
		}
		else if (ipclass == 'classC' && $('#opctx').val() > Math.pow(2, 8)){
			alert ("El numero de Hosts no debe sobrepasar "+Math.pow(2, 8));
		}
		else{
			calculate ();
			redPrivada ();
			$('#resultados, #output').fadeIn();
		}
	}
	else if ($('#opc_sub').is(':checked')) {
		if (ipclass == 'classA' && $('#opctx').val() > Math.pow(2, 23)){
			alert ("El numero de Subredes no debe sobrepasar "+Math.pow(2, 23));
		}
		else if (ipclass == 'classB' && $('#opctx').val() > Math.pow(2, 15)){
			alert ("El numero de Subredes no debe sobrepasar "+Math.pow(2, 15));
		}
		else if (ipclass == 'classC' && $('#opctx').val() > Math.pow(2, 7)){
			alert ("El numero de Subredes no debe sobrepasar "+Math.pow(2, 7));
		}
		else{
			calculate ();
			redPrivada ();
			$('#resultados, #output').fadeIn();
		}
	}
	else if ($('#opc_prefx').is(':checked')) {
		if ($('#opctx').val() > 30){
			alert ("El prefijo debe ser menor o igual a 30");
		}
		else if (ipclass == 'classA' && $('#opctx').val() < 8){
			alert ("El prefijo debe ser mayor a 8");
		}
		else if (ipclass == 'classB' && $('#opctx').val() < 16){
			alert ("El prefijo debe ser mayor a 16");
		}
		else if (ipclass == 'classC' && $('#opctx').val() < 24){
			alert ("El prefijo debe ser mayor a 24");
		}
		else{
			calculate ();
			redPrivada ();
			$('#resultados, #output').fadeIn();
		}
	}
}

function redPrivada (){
	if (ip[0] == 127 && hosts_bits <= 24){
		$('#resultados').append(' (Red Privada)');
	}
	else if (ip[0] == 10 && hosts_bits <= 24){
		$('#resultados').append(' (Red Privada)');
	}
	else if (ip[0] == 172 && ip[1] == 16 && hosts_bits <= 12){
		$('#resultados').append(' (Red Privada)');
	}
	else if (ip[0] == 192 && ip[1] == 168 && hosts_bits <= 16){
		$('#resultados').append(' (Red Privada)');
	}
}

function calculate (){
	if ($('#opc_hosts').is(':checked')){ // Si el radio Hosts está seleccionado

		var hosts = parseInt($('#opctx').val());

		//Calculo de numero de Hosts posibles
		for (var i = 1; i <= bits_disp; i++) {
			if (Math.pow(2, i)-2 >= hosts) {
				numhosts = Math.pow(2, i)-2;
				hosts_bits = i;
				break;
			}
		}

		//Calculo de subredes posibles
		sub_bits = bits_disp - hosts_bits;
		if (sub_bits != 0){
			numsub = Math.pow(2, sub_bits);
		}
		else{
			numsub = 1;
		}

		maskgen (bits_disp, sub_bits);
	}
	else if ($('#opc_sub').is(':checked')){ // Si el radio Hosts está seleccionado

		var subs = parseInt($('#opctx').val());

		//Calculo de numero de subredes posibles
		for (var i = 1; i <= bits_disp; i++) {
			if (Math.pow(2, i) >= subs) {
				numsub = Math.pow(2, i);
				sub_bits = i;
				break;
			}
		}

		//Calculo de hosts posibles
		hosts_bits = bits_disp - sub_bits;
		if (hosts_bits != 0){
			numhosts = Math.pow(2, hosts_bits);
		}
		else {
			numhosts = 0;
		}

		maskgen (bits_disp, sub_bits);
	}
	else if ($('#opc_prefx').is(':checked')){ // Si el radio Hosts está seleccionado

		sub_bits = parseInt($('#opctx').val())-(32-bits_disp);

		//Calculo de hosts posibles
		if (sub_bits != 0){
			numsub = Math.pow(2, sub_bits);
		}
		else {
			numsub = 1;
		}

		hosts_bits = bits_disp-sub_bits;
		numhosts = Math.pow(2, hosts_bits)-2;

		maskgen (bits_disp, sub_bits);
	}

	calcsub (bits_disp, sub_bits);
 
	$('#resultados').html('Hosts X Subred: '+numhosts+'  -  Bits: '+hosts_bits+'<br/>Subredes: '+numsub+'  -  Bits: '+sub_bits+'<br/>Máscara de Subred: '+mascara);
}

function maskgen (bits_disp, sub_bits){
	//Generar mascara de subred
	mascara = '';
	var sum = 0;
	for (var j = 0; j < (32-bits_disp+sub_bits); ) {
		for (var i = 7; i >= 0 && j < (32-bits_disp+sub_bits); i--) {
			sum += Math.pow(2, i);
			j++;
		}
		mascara += sum;
		if (j % 8 == 0) {
			mascara += '.';
		}
		sum = 0;
	}
	while (j < 32) {
		if (j % 8 == 0) {
			mascara += '.'+0;
		}
		j++;
	}
}

function calcsub (bits_disp, sub_bits){

	var array = new Array(bits_disp/8);
	var string_ip = new Array(bits_disp/8);
	for (i = 0; i < bits_disp/8; i++){
		array[i] = new Array(8);
		string_ip[i] = 0;
	}
	var binary;
	var sum;
	var pos;
	var lim_b;
	var i, j;

	// posicion inicio de bits de subred
	var lim_a = parseInt(sub_bits/8);
	var buffer = '';
	$('#output').html('<p>Subredes:</p>');

	for (var a = 0; a < numsub; a++) {	//Numero de Subredes
		for (j = 1; j <= ((32-bits_disp)/8); j++) {	// Octetos fijos de clase
			buffer += $('#ip'+j).val()+'.';  
		}
	
		sum = 0;	
		binary = dec2bin (a);
		pos = binary.length;
		lim_b = 8-(sub_bits%8);
		for (i = lim_a; i >= 0; i--) {
			if (pos == 0)
				break;
			for (j = lim_b; j < 8; j++) {
				pos--;
				array [i][j] = parseInt(binary.charAt (pos));
				sum += Math.pow(2, j)*parseInt(array [i][j]);
				if (pos == 0)
					break;
			}
			string_ip[i] = sum;
			sum = 0;
			lim_b = 0;
		}
		for (i = 0; i < bits_disp/8; i++) {
			buffer += string_ip[i]+'.';
		}
		$('#output').append(a+': '+buffer.link('#')+'<br/>'); // Escribir en el html el link
		buffer = new String();
	}

	

	$('a').click(function() {
		$('#output2').hide();
		for (var i = 0; i < 4; i++) {
			ip[i] = '';
		}
		var i = 0;
		var j = 0;
		var linktext = this.text;
		$('#output2').html('<p>'+'Subred: '+linktext+'<br>Hosts:</p>');

		//Pasar texto de link a arreglo
		while(i < linktext.length && j < 4) {
			if (linktext.charAt(i) != '.'){
				ip[j] += linktext.charAt(i);
			}
			else
				j++;
			i++;
		}
		var new_ip = new Array(4);
		for (var i = 0; i < 4; i++) {
			new_ip[i] = parseInt(ip[i]);
		}

		var hostip = new Array (parseInt(hosts_bits / 8)+1);
		for (var a = 1; a < numhosts+1; a++) {
			buffer = '';
			binary = dec2bin(a);
			pos = binary.length;
			for (i = 0; i < parseInt(hosts_bits / 8)+1; i++) {
				hostip[i] = 0;
				if (pos == 0)
					break;
				for (j = 0; j < 8; j++) {
					if (pos == 0)
						break;
					hostip[i] += Math.pow(2, j)*parseInt(binary.charAt(pos-1));
					pos--;
				}
				new_ip[3-i] = hostip[i]+parseInt(ip[3-i]);
			}
			buffer = new_ip[0]+'.'+new_ip[1]+'.'+new_ip[2]+'.'+new_ip[3];
			$('#output2').append(a+': '+buffer.link('#')+'<br/>'); // Escribir en el html el link
			$('#output2').fadeIn();
		}
		
	});
}


